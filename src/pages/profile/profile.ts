import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Profile } from '../../models/profile';
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  existingProfile = {} as Profile;
  constructor( private auth:AuthProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  getExistingProfile(profile: Profile) {
    this.existingProfile = profile;
  }

  navigateToEditProfilePage() {
    console.log(this.existingProfile);
    this.navCtrl.push('EditProfilePage', { existingProfile: this.existingProfile })
  }

  signOut(){
    this.auth.signOut();
    window.location.reload()
    // this.navCtrl.setRoot('LoginPage')
  }
}
