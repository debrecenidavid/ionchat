import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  inboxRoot = 'InboxPage'
  channelsRoot = 'ChannelsPage'
  profileRoot = 'ProfilePage'


  constructor(public navCtrl: NavController) {}

}
