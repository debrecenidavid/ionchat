import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InboxPage } from './inbox';
import { OnlineUsersComponent } from '../../components/online-users/online-users';

@NgModule({
  declarations: [
    InboxPage,
    OnlineUsersComponent
  ],
  imports: [
    IonicPageModule.forChild(InboxPage),
  ],
})
export class InboxPageModule {}
