import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchUserPage } from './search-user';
import { ProfileSearchComponent } from '../../components/profile-search/profile-search';

@NgModule({
  declarations: [
    SearchUserPage,
    ProfileSearchComponent
  ],
  imports: [
    IonicPageModule.forChild(SearchUserPage),
  ],
})
export class SearchUserPageModule {}
