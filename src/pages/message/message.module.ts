import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MessagePage } from './message';
import { SendMessageBoxComponent } from '../../components/send-message-box/send-message-box';
import { ChatMessageComponent } from '../../components/chat-message/chat-message';

@NgModule({
  declarations: [
    MessagePage,
    SendMessageBoxComponent,
    ChatMessageComponent
  ],
  imports: [
    IonicPageModule.forChild(MessagePage),
  ],
})
export class MessagePageModule {}
