import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import { Observable } from 'rxjs';
import { Channel } from '../../models/channel';


@IonicPage()
@Component({
  selector: 'page-channels',
  templateUrl: 'channels.html',
})
export class ChannelsPage {

  private loader:Loading;
  channelList$: Observable<Channel[]>;
  constructor(private loading:LoadingController, private chat:ChatProvider, private alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    this.loader=loading.create({
      content: "Loading channels..."
    })
  }

  ionViewWillLoad(){
    this.loader.present();
    this.getChannels()
  }
  
  getChannels(){
    this.channelList$ = this.chat.getChannelListRef();
    this.channelList$.subscribe((asd)=>{
      console.log(asd);
      this.loader.dismiss();
    })
  }

  selectChannel(channel:Channel){
    console.log(channel);
    this.navCtrl.push('ChannelChatPage',{channel})
  }
  showAddChannelDialog() {
    this.alertCtrl.create({
      title:'Channel name',
      inputs:[{
        name:'channelName'
      }],
      buttons:[
        {
          text:'Cancel',
          role:'cancel'
        },
        {
          text:'Add',
          handler:data=>{
            this.chat.addChannel(data.channelName)
          }
        }
      ]
    }).present()
  }

}
