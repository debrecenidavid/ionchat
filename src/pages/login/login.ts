import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { LoginResponse } from '../../models/login-response';
import { DataProvider } from '../../providers/data/data';
import { User } from 'firebase/app';
import { Subscription } from 'rxjs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private profile$: Subscription;
  constructor(private toast:ToastController,private data:DataProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  login(event:LoginResponse){
    if(!event.error){
      this.toast.create({
        message: `Welcome to Beep: ${event.result.email}`,
        duration: 3000
      }).present()
     this.profile$ = this.data.getProfile(<User>event.result).subscribe(profile=>{
        profile ? this.navCtrl.setRoot('TabsPage'):this.navCtrl.setRoot('EditProfilePage');
      })
    }else{
      this.toast.create({
        message: event.error.message,
        duration: 3000
      }).present()
    }
  }
  ionViewWillUnload() {
    console.log("destroyed");
    if(this.profile$){
      this.profile$.unsubscribe()
    }
  }


}
