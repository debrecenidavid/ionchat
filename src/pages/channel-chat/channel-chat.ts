import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Channel } from '../../models/channel';
import { ChatProvider } from '../../providers/chat/chat';
import { Observable } from 'rxjs';
import { ChannelMessage } from '../../models/channel-message';

@IonicPage()
@Component({
  selector: 'page-channel-chat',
  templateUrl: 'channel-chat.html',
})
export class ChannelChatPage {

  channelMessages$: Observable<ChannelMessage[]>;
  channel: Channel;
  constructor(private chat:ChatProvider, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillLoad() {
    this.channel = this.navParams.get('channel');
    this.channelMessages$=this.chat.getChannelChatRef(this.channel.key);
  }

  sendMessage(message:string){
    let channelMessages:ChannelMessage ={
      content:message
    }
    this.chat.sendChannelChatMessage(this.channel.key,channelMessages);
  }

}
