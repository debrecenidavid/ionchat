import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChannelChatPage } from './channel-chat';
import { SendMessageBoxComponent } from '../../components/send-message-box/send-message-box';

@NgModule({
  declarations: [
    ChannelChatPage,
    SendMessageBoxComponent
  ],
  imports: [
    IonicPageModule.forChild(ChannelChatPage),
  ],
})
export class ChannelChatPageModule {}
