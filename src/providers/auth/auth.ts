import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Account } from '../../models/account';
import { LoginResponse } from '../../models/login-response';

@Injectable()
export class AuthProvider {

  constructor(private afAuth:AngularFireAuth) {
  }

  getAuthenticatedUser(){
    return this.afAuth.authState;
  }
  async createUserWithEmailAndPassword(account:Account){
    try {
      const res = await this.afAuth.auth
      .createUserWithEmailAndPassword(account.email,account.password);
      return <LoginResponse> {
        result: res.user
      }
    } catch (err) {
      return <LoginResponse> {
        error: err
      }
    }
  }
  async signInWithEmailAndPassword(account:Account) {
    try {
      const res = await this.afAuth.auth.
        signInWithEmailAndPassword(account.email, account.password);
      return <LoginResponse> {
        result: res.user
      }
    } catch (err) {
      return <LoginResponse> {
        error: err
      }
    }
  }

  signOut(){
    this.afAuth.auth.signOut();
  }

}
