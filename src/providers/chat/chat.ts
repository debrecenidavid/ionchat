import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Channel } from '../../models/channel';
import { ChannelMessage } from '../../models/channel-message';
import { map } from 'rxjs/operators';

@Injectable()
export class ChatProvider {

  channelList$: AngularFireList<Channel>;
  channelMessages$: AngularFireList<ChannelMessage>;
  constructor(private database: AngularFireDatabase) {
  }

  addChannel(channelName: string) {
    this.database.list(`/channel-names/`).push({ name: channelName })
  }

  getChannelListRef() {
    this.channelList$ = this.database.list(`channel-names`);
    return this.channelList$.snapshotChanges().pipe(map(
      changes=>{
        return changes.map(c=>({
          key:c.payload.key, ... c.payload.val()
        }))
      }
    ))
  }

  getChannelChatRef(channelKey: string) {
    this.channelMessages$ = this.database.list(`channels/${channelKey}`);
    return this.channelMessages$.snapshotChanges().pipe(map(
      changes=>{
        return changes.map(c=>({
          key:c.payload.key, ... c.payload.val()
        }))
      }
    ))
  }

  async sendChannelChatMessage(channelKey: string, message: ChannelMessage) {
    await this.database.list(`channels/${channelKey}`).push(message);
  }
}
