import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject, AngularFireList } from 'angularfire2/database';
import { User, database } from 'firebase/app'
import { Profile } from '../../models/profile';
import { AuthProvider } from '../auth/auth';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class DataProvider {

  profileObject$: AngularFireObject<Profile>;
  profileList$: AngularFireList<Profile>;


  private authenticatedUser$: Subscription;

  constructor(private database: AngularFireDatabase, private auth: AuthProvider) {
  }

  searchUser(firstName: string) {
    this.profileList$ = this.database.list('/profiles', ref =>
      ref.orderByChild('firstName')
        //  .equalTo(firstName)
        .startAt(firstName)
        .endAt(firstName + "\uf8ff")
    );
    return this.profileList$.snapshotChanges().pipe(map(
      changes=>{
        return changes.map(c=>({
          key:c.payload.key, ... c.payload.val()
        }))
      }
    ))
  }
  getProfile(user: User) {
    this.profileObject$ = this.database.object(`/profiles/${user.uid}`);
    return this.profileObject$.snapshotChanges().pipe(map(action => {
      const key = action.payload.key;
      const data = { key, ...action.payload.val() };
      return data;
    }));
  }
  async saveProfile(user: User, profile: Profile) {
    this.profileObject$ = this.database.object(`/profiles/${user.uid}`);
    try {
      await this.profileObject$.set(profile);
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  getOnlineUsers(){
    this.profileList$ = this.database.list(`online-users`);
    return this.profileList$.snapshotChanges().pipe(map(
      changes=>{
        return changes.map(c=>({
          key:c.payload.key, ... c.payload.val()
        }))
      }
    ))
  }
  setUserOnline(profile: Profile) {
    const ref = database().ref(`online-users/${profile.key}`)

    try {
      ref.update({ ...profile });
      ref.onDisconnect().remove();
    } catch (error) {
      console.error(error);
    }
  }

}
