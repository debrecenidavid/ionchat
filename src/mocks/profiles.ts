import { Profile } from "../models/profile";

const userList:Profile[]=[
    {
        firstName:'Paul',
        lastName:'Halliday',
        email:'paul@paul.com',
        avatar:'assets/imgs/avatar.png',
        dateOfBirth: new Date()
    },
    {
        firstName:'John',
        lastName:'Doe',
        email:'John@John.com',
        avatar:'assets/imgs/avatar.png',
        dateOfBirth: new Date()
    },
    {
        firstName:'Gipsz',
        lastName:'Jakab',
        email:'Gipsz@Gipsz.com',
        avatar:'assets/imgs/avatar.png',
        dateOfBirth: new Date()
    },
    {
        firstName:'Fox',
        lastName:'Fast',
        email:'Fox@Fox.com',
        avatar:'assets/imgs/avatar.png',
        dateOfBirth: new Date()
    }
];

export const USER_LIST = userList;