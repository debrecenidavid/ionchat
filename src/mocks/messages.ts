import { Profile } from "../models/profile";
import { Message } from "../models/message"
import { USER_LIST } from "./profiles";

const userList=USER_LIST;
const messageList: Message[]=[];

userList.forEach(user=>{
    messageList.push({
        user:user,
        date:new Date(),
        lastMessage:"Hello from the oth.."
    })
    messageList.push({
        user:user,
        date:new Date(),
        lastMessage:"Wazzuuup"
    })
})

export const MESSAGE_LIST = messageList;