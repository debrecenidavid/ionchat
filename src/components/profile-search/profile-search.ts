import { Component, Output, EventEmitter } from '@angular/core';
import { DataProvider } from '../../providers/data/data';
import { Profile } from '../../models/profile';
import { Subscription } from 'rxjs';

@Component({
  selector: 'profile-search',
  templateUrl: 'profile-search.html'
})
export class ProfileSearchComponent {

  query: string;
  profileList: Profile[];

  @Output() selectedProfile: EventEmitter<Profile>;

  profileList$: Subscription;

  constructor(private data: DataProvider) {
    this.selectedProfile = new EventEmitter<Profile>();
  }

  searchUser(query: string) {
    const trimmedQuery = query.trim();
    if (trimmedQuery === query && query.length > 0) {
      this.profileList$ = this.data.searchUser(query)
        .subscribe((profiles: any) => {
          this.profileList = profiles;
        })
    }
  }

  selectProfile(profile: Profile) {
    this.selectedProfile.emit(profile);
  }
  ionViewWillUnload() {
    this.profileList$.unsubscribe()
  }
}
