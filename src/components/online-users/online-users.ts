import { Component } from '@angular/core';
import { DataProvider } from '../../providers/data/data';
import { Subscription, Observable } from 'rxjs';
import { Profile } from '../../models/profile';
import { AuthProvider } from '../../providers/auth/auth';
import { User } from 'firebase';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'online-users',
  templateUrl: 'online-users.html'
})
export class OnlineUsersComponent {

  private authenticatedUser$: Subscription;
  private profile$: Subscription;

  onlineUsers$: Observable<Profile[]>;


  constructor(private navCtrl:NavController, private data: DataProvider, private auth: AuthProvider) {
  }

  ngOnInit(): void {
    this.setUserOnline();
    this.getOnlineUsers();
  }
  getOnlineUsers() {
    this.onlineUsers$ = this.data.getOnlineUsers();
  }
  setUserOnline() {
    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.profile$ = this.data.getProfile(user).subscribe((profile: Profile) => {
        this.data.setUserOnline(profile)
        this.authenticatedUser$.unsubscribe()
        this.profile$.unsubscribe()
        console.log(profile);
      })
    })
  }

  openChat(profile:Profile){
    this.navCtrl.push('MessagePage',{profile})
  }

}
