import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'send-message-box',
  templateUrl: 'send-message-box.html'
})
export class SendMessageBoxComponent {

  @Output() sendMessage:EventEmitter<string>;
  content:string;

  constructor() {
    this.sendMessage=new EventEmitter<string>()
  }

  send(){
    this.sendMessage.emit(this.content)
    this.content=""
  }

}
