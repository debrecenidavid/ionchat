import { Component, EventEmitter, Output } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Account } from '../../models/account';
import { LoginResponse } from '../../models/login-response';
import { AuthProvider } from '../../providers/auth/auth';


@Component({
  selector: 'login-form',
  templateUrl: 'login-form.html'
})
export class LoginFormComponent {

  account = {} as Account;
  @Output() loginStatus: EventEmitter<LoginResponse>;
  constructor(private authService: AuthProvider, private navCtrl: NavController) {
    this.loginStatus = new EventEmitter<LoginResponse>();
  }

  async login() {
    const LoginResponse = await this.authService.signInWithEmailAndPassword(this.account);
    this.loginStatus.emit(LoginResponse);
  }
  navigateToRegisterPage() {
    this.navCtrl.push("RegisterPage");
  }

}
