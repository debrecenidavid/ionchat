import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Profile } from '../../models/profile';
import { DataProvider } from '../../providers/data/data';
import { AuthProvider } from '../../providers/auth/auth';
import { Subscription } from 'rxjs';
import { User } from 'firebase/app';
@Component({
  selector: 'edit-profile-form',
  templateUrl: 'edit-profile-form.html'
})
export class EditProfileFormComponent {

  private authenticatedUser$: Subscription;
  private authenticatedUser: User;

  @Output() saveProfileResult: EventEmitter<Boolean>

  @Input() profile: Profile;
  constructor(private data: DataProvider, private authService: AuthProvider) {
    this.saveProfileResult = new EventEmitter<Boolean>();
    this.authenticatedUser$ = this.authService.getAuthenticatedUser().subscribe((user: User) => {
      this.authenticatedUser = user;
    })
  }

  ngOnInit(): void {
    console.log(this.profile);
    if(!this.profile){
      this.profile = {} as Profile;
    }
  }

  ionViewWillUnload() {
    console.log("destroyed");
    this.authenticatedUser$.unsubscribe()
  }

  async saveProfile() {
    if (this.authenticatedUser) {
      this.profile.email = this.authenticatedUser.email;
      const result = await this.data.saveProfile(this.authenticatedUser, this.profile)
      this.saveProfileResult.emit(result)
    }
  }
}
