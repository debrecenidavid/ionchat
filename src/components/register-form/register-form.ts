import { Component, Output, EventEmitter } from '@angular/core';
import { Account } from '../../models/account';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginResponse } from '../../models/login-response';

@Component({
  selector: 'register-form',
  templateUrl: 'register-form.html'
})
export class RegisterFormComponent {

  account = {} as Account;

  @Output() registerStatus:EventEmitter<LoginResponse>
  constructor(private authService:AuthProvider) {
    this.registerStatus=new EventEmitter<LoginResponse>();
  }

  async register() {
    try {
      const result = await this.authService
      .createUserWithEmailAndPassword(this.account);
      this.registerStatus.emit(result);

    } catch (err) {
      console.error(err);
      this.registerStatus.emit(err);
    }
  }

}
