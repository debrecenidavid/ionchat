import { Component, Output, EventEmitter } from '@angular/core';
import { DataProvider } from '../../providers/data/data';
import { AuthProvider } from '../../providers/auth/auth';
import { User } from 'firebase';
import { Profile } from '../../models/profile';
import { LoadingController, Loading } from 'ionic-angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'profile-view',
  templateUrl: 'profile-view.html'
})
export class ProfileViewComponent {

  public userProfile: Profile
  private loader: Loading

  @Output() existingProfile: EventEmitter<Profile>;

  private authenticatedUser$: Subscription;
  private profile$: Subscription;
  constructor(private loading: LoadingController, private data: DataProvider, private auth: AuthProvider) {
    this.existingProfile = new EventEmitter<Profile>();

    this.loader = loading.create({
      content: "Loading profile..."
    })
  }

  ngOnInit(): void {
    this.loader.present();
    this.authenticatedUser$ = this.auth.getAuthenticatedUser().subscribe((user: User) => {
      this.profile$ = this.data.getProfile(user).subscribe((profile: Profile) => {
        this.userProfile = profile;
        this.existingProfile.emit(this.userProfile)
        this.loader.dismiss();
        this.authenticatedUser$.unsubscribe()
        this.profile$.unsubscribe()
        console.log(this.userProfile);
      })
    })

    
  }
}
